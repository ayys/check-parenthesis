/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Use stack to check for parenthesis mistake in code
 *
 *        Version:  1.0
 *        Created:  2017 फेब्रुअरी 21 मङ्गलबार  20:50:43
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ayush Jha (aj), ayushjha6@gmail.com
 *        Company:  
 *
 * =====================================================================================
 */

#include "stack/stack.h"
#include "main.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>



char * get_input_string(int argc, char * argv[]){
  if (argc > 1){
    int c, len = 0;
    char *out_str;
    for (c = 1; c < argc; c++){
      len += strlen(argv[c]);
      out_str = malloc(len * sizeof(char) + 1);
      strcat(out_str, argv[c]);
    }
    return out_str;
  }
  return NULL;
}

int measure_parenthesis(char c, Stack *stack){
  if (c == '('){
    stack->push(&OPEN_PAREN, stack);
    return 1;
  }
  else if (c == ')'){
    stack->pop(stack->mindex, stack);
    return -1;
  }
  return 0;
}


int check_paren_validity(Stack *stack){
  if (stack->mindex == 0)
    return 0;
  else{
    if (stack->mindex < 0)
      return -1;
    else /* if mindex > 0 */
      return 1;
  }
}

int main(int argc, char *argv[]){
  /* create a stack */
  Stack stack = new_stack();
  int paren_validity; /* stores the equalibrium of parenthesis */

  /* get the input string */
  char *input = get_input_string(argc, argv);
  if (input == NULL){
    fprintf(stderr, "Not enough arguments!\n");
    return -1;
  }

  for (int i = 0; input[i] != '\0'; i++){
    measure_parenthesis(input[i], &stack);
  }

  paren_validity = check_paren_validity(&stack);
  if (paren_validity == 0)
    printf(PAREN_OKAY_TEXT);
  else if (paren_validity == -1)
    printf(PAREN_NOT_OPENED_TEXT);
  else
    printf(PAREN_NOT_CLOSED_TEXT);
  return 0;
}
