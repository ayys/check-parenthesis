#include <stdio.h>

/*
 * A single element in the array
 */
struct __stack_element__ {
  /* The int value stored in the stack */
  void *data;
  /* pointer to next term : NULL if it's the last item */
  struct __stack_element__ *next;
};


/*
 * declaration of the Stack struct shell
 */
struct __stack__{
  /*
   * Number of elements in the stack - maximum index
   */
  int mindex;
  
  /* pointer to the first element in the stack */
  struct __stack_element__ *base;
  /* push element into the stack - takes element as input */
  struct __stack__ * (*push)(void *,struct __stack__ *);
  /* pop element from the stack - takes index as input */
  int (*pop)(int, struct __stack__ *);
  /* returns the nth element in the stack */
  struct __stack_element__* (*traverse)(int, struct __stack__*);
};

typedef struct __stack__ Stack;
typedef struct __stack_element__ StackElement;

/*
 * function declarations
 * */

/*
 * function to create a new stack
 * */
Stack new_stack();
/*
 * remove an element from the stack at the given index
 * */
int pop(int, Stack*);
/*
 * insert the given data into the stack
 * */
Stack * push(void *, Stack*);
/*
 * traverse through the stack upto the given index
 * */
StackElement * traverse(int, Stack *);
