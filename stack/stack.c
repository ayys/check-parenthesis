#include "stack.h"
#include <stdlib.h>


Stack * push(void *data, Stack * stack){
  /*
   * If the stack is empty, i.e stack->base is null, then add to top
   * else add a new stackelement and put it above top
   * Note that the new element is added above all the others
   */
  if (stack->mindex == 0){
    stack->base = malloc(sizeof(StackElement));
    if (stack->base == NULL){
      fprintf(stderr, "Out of memory!!");
      return NULL;
    }
    stack->base->data = data;
    stack->base->next = NULL;
  }
  else{
    /*
     * add the new element on top off the base,
     * declare the new element as the base, so that the next element
     * will be added on top of the latest element
     */
    StackElement *tmp = malloc(sizeof(StackElement));
    if (tmp == NULL){
      fprintf(stderr, "Out of memory!!");
      return NULL;
    }
    tmp->data= data;
    tmp->next = stack->base;
    stack->base = tmp;
  }
  stack->mindex ++;
  return stack;
}


int pop(int index, Stack* stack){
  /*
   * check if the index is in bounds of the stack
   */
  if (index > stack->mindex)
    return -1;
  /*
   * if the index is 0, pop the top of the stack
   */
  StackElement *tmp = malloc(sizeof(StackElement));
  if (tmp == NULL){
    fprintf(stderr, "Out of memory!!");
    return -1;
  }
  StackElement *tmp_prev = malloc(sizeof(StackElement));
  if (tmp_prev == NULL){
    fprintf(stderr, "Out of memory!!");
    return -1;
  }
  tmp = stack->base;
  int c;
  for (int c = 0; c < index;c++){
    if (tmp == NULL || tmp->next == NULL)
      break;
    tmp = tmp->next;
  }
  for (int c = 0; c < index-1; c++){
    if (tmp_prev == NULL|| tmp_prev->next == NULL)
      break;
    tmp_prev = tmp_prev->next;
  }
  tmp_prev->next = tmp->next;
  stack->mindex --;
  free(tmp);
  return index;
}


StackElement * traverse(int index, Stack *stack){
  /* return the stack element at the indexth position */
  if (index >= stack->mindex)
    return NULL;
  int c;
  StackElement *tmp = malloc(sizeof(StackElement));
  if (tmp == NULL){
    fprintf(stderr, "Out of memory!!");
    return NULL;
  }
  tmp = stack->base;
  for(c = 0; c < index; c++)
    tmp = tmp->next;
  return tmp;
}


Stack new_stack(){
  /* Create a new stack object and return it */
  Stack new;
  new.mindex = 0;
  new.base = NULL;
  new.push = &push;
  new.pop = &pop;
  new.traverse = &traverse;
  return new;
}
