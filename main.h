/*
 * =====================================================================================
 *
 *       Filename:  main.h
 *
 *    Description:  header file for main.c
 *
 *        Version:  1.0
 *        Created:  2017 फेब्रुअरी 21 मङ्गलबार  21:20:36
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ayush Jha (aj), ayushjha6@gmail.com
 *        Company:  
 *
 * =====================================================================================
 */


char OPEN_PAREN = '(';
char *PAREN_OKAY_TEXT = "Parenthesis are okay\n";
char *PAREN_NOT_OPENED_TEXT = "Some Parenthesis are closed, but not opened!\n";
char *PAREN_NOT_CLOSED_TEXT = "Some parenthesis are opened, but not closed!\n";


/* 
 * check if there are enough arguments,
 * if there are, then concatinate all the arguments into one
 * and return a string containing the concatinated arguments
 * */
char * get_input_string(int, char **);


/* 
 * if there is an open-paren, push it to stack
 * if there is a close-paren, pop a open-paren from stack
 * else do nothing
 * */
int measure_parenthesis(char, Stack *);


/* 
 * if parens are equal, return 0
 * if close-parens are more return -1
 * if open-parens are more return 1
 * */
int check_paren_validity(Stack *);
