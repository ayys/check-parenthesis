# check-parenthesis
Check Parenthesis using Stacks in C

## Example

```bash
    $ cparen "(( 1 + 23 + (34 + 45) ))" 
    Parenthesis are okay
```
